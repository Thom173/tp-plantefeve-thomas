const NB_CHIFFRES = 17;


for(var I=0;I<=NB_CHIFFRES;I++) {
    document.write("Fibonacci("+I+")="+Fibonacci(I)+"<BR>");
}

function Fibonacci(prmNombre) {
    var somme;
    if(prmNombre <= 0) return 0;
    if(prmNombre == 1) return 1;
    var n1 = 0;
    var n2 = 1;
    for(var i=2; i <= prmNombre; i++) {
        somme = n1+n2;
        n1 = n2;
        n2 = somme;
    };
    return n2;
 }