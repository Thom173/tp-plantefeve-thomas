$(document).ready(function () {

    /* Gestionnaire d'évènement CLICK sur le bouton "Ajouter" */ 
    $("#btnAdd").click(function() {
        //ajout d'une ligne à la fin de la partie TBODY
        let numligne = $("#tab1 tbody tr").length +1;
        $("#tab1 tbody").append("<tr><td>"+ numligne+"</td></tr>");
        calcultotal();
    });

    /* Gestionnaire d'évènement CLICK sur le bouton "Supprimer" */ 
    $("#btnSuppr").click(function() {
        //suppression de la dernière ligne de la partie TBODY
        $("#tab1 tbody tr:last").remove();
        calcultotal();
    });

    function calcultotal(){
     //faire le calcul de tous les tr td
    let somme = 0;
    $("#tab1 tbody tr td").each(function (index) {
          let num = parseInt($(this).html());
            somme += num;
    });
     //afficher dans tfoot
    $("#tab1 tfoot tr td").html(somme);
    }
});